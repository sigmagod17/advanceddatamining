# -*- coding: utf-8 -*-
import os
import codecs
import sys
from googleplaces import GooglePlaces, types, lang
import random


mylist = ['AIzaSyD_iWucRw8uGOJ303PfOY0-Mh2g94qqRVk',
          'AIzaSyDvm61Y0_zo6zFYIeRWAt4ZzuLR7hpAZPQ',
          'AIzaSyBwxmH6M7nE97yT8lJ4IrS2zXuGjdHaT-0']

reload(sys)

sys.setdefaultencoding('utf-8')

with codecs.open(os.getcwd()+'/DataSet.txt','r',encoding='utf8') as f:
    f_build = f.readlines()

header = f_build[0]
schema = header.split('\t')
idx = 0
for col in schema:
    print idx,':',col
    idx += 1

datas = []
for row in f_build[1:]:
    data = row.split('\t')
    datas.append(data)

mydatas = []
for row in datas:

    if row[4] == '住' and row[3] > 0 and row[1] != '土地' and (('層' in row[9] and '夾層' not in row[9]) or '全部' in row[9]):
        mydatas.append(row)

print '全部:', len(datas)
print '過濾完:', len(mydatas)

#參數kind,1代表傳入地址，回傳代號、2代表傳入代號，回傳地址
def get_address(kind,address):
    address_nm = ['中區','東區','南區','西區','北區','北屯區','西屯區','南屯區','太平區','大里區','霧峰區','烏日區','豐原區',
                  '后里區','石岡區','東勢區','和平區','新社區','潭子區','大雅區','神岡區','大肚區','沙鹿區','龍井區','梧棲區',
                 '清水區','大甲區','外埔區','大安區']
    address_no =['400','401','402','403','404','406','407','408','411','412','413','414','420','421','422','423','424',
                 '426','427','428','429','432','433','434','435','436','437','438','439']
    #比對地區回傳地區代號，若沒有回傳-1
    r = '0'
    for i in range(len(address_no)):
        if kind == '1':
            if address.find(address_nm[i]) != -1:
                r = address_no[i]
        elif kind == '2':
             if address == address_no[i]:
                r = '台中市'+address_nm[i]
    return r

#傳入地址找鄰近交通、學校
def count_nearby(address):
    API_KEY = random.choice(mylist)
    google_places = GooglePlaces(API_KEY)
    try:
        query_result = google_places.nearby_search(
            location=address, keyword='', radius=1000,
            types=[types.TYPE_TRAIN_STATION, types.TYPE_SUBWAY_STATION])
        # 有雙向問題 會算兩次 types.TYPE_BUS_STATION

        if len(query_result.places) > 0:
            return u'有'
        else:
            return u'無'
    except:
        return u'無'

        # except Exception as e:
    #     print e
    #     if google_places.RESPONSE_STATUS_OK == 'OVER_QUERY_LIMIT':
    #         print 'OVER_QUERY_LIMIT'
    #
    #         count_nearby(address)
    #     else:
    #         return u'無'

#參數kind,1代表傳入車位種類名稱，回傳代號、2代表傳入代號，回傳車位種類名稱
def get_park(kind,park):
    park_nm = ['一樓平面','升降平面','升降機械','坡道平面','坡道機械','其他']
    park_no =['1','2','3','4','5','6']
    #比對車位種類回傳代號，若沒有回傳0
    r = '0'
    for i in range(len(park_no)):
        if kind == '1':
            if park.find(park_nm[i]) != -1:
                r = park_no[i]
        elif kind == '2':
             if park == park_no[i]:
                r = park_nm[i]
    return r

from dateutil import relativedelta
from datetime import datetime

#回傳屋齡
def get_years(builddate):
    if builddate != '':
        builddate = builddate.rjust(7, '0')
        builddate = str(int(builddate[0:3]) + 1911) + builddate[3:7]
        house_aged = datetime.strptime('20161231', '%Y%m%d').date() - datetime.strptime(builddate, '%Y%m%d').date()
        return round(house_aged.days * 1.00 / 365, 2)

def get_layer(layer,encoding="utf8"):
    chs_arabic_map = {u'零': 0, u'一': 1, u'二': 2, u'三': 3, u'四': 4,
                      u'五': 5, u'六': 6, u'七': 7, u'八': 8, u'九': 9,
                      u'十': 10, u'百': 100, u'千': 10 ** 3, u'萬': 10 ** 4,
                      u'0': 0, u'1': 1, u'２': 2, u'3': 3, u'4': 4,
                      u'5': 5, u'6': 6, u'7': 7, u'8': 8, u'9': 9}

    if isinstance(layer, str):
        layer = layer.decode(encoding)

    if u'層' in layer:
        layers = layer[:-1]
        result = 0
        tmp = 0
        hnd_mln = 0
        for count in range(len(layers)):
            curr_char = layers[count]
            curr_digit = chs_arabic_map.get(curr_char, None)
            if curr_digit == 10 ** 4:
                result = result + tmp
                result = result * curr_digit
                tmp = 0
            elif curr_digit >= 10:
                tmp = 1 if tmp == 0 else tmp
                result = result + curr_digit * tmp
                tmp = 0
            # meet single digit
            elif curr_digit is not None:
                tmp = tmp * 10 + curr_digit
            else:
                return result
        result = result + tmp
        result = result + hnd_mln
        return result
    elif u'全' in layer:
        return 999
    else:
        return  -1


# 轉換有/無成Y/N的方法 (包含反解)
to_flag = {}
to_flag[u'有'] = 'Y'
to_flag[u'無'] = 'N'
to_flag['Y'] = u'有'
to_flag['N'] = u'無'


def get_have_flag(char):
    return to_flag[char]


def cal_levels(datas, colidx, dis):
    min_price = min(x[colidx] for x in filter_datas)
    max_price = max(x[colidx] for x in filter_datas)
    unit = len(filter_datas) / dis
    temp = sorted(filter_datas, key=itemgetter(colidx))
    levels = []
    for i in range(dis):
        levels.append(temp[unit * i][colidx])
    return levels


# 轉換成坪數區間
def convert_square(square, levels):
    return convert_value('area', square, levels)


def convert_floor(total, floor, levels):
    if total:
        return convert_value('total floor', floor, levels)
    else:
        return convert_value('floor', floor, levels)


def convert_house_age(age, levels):
    return convert_value('years', age, levels)


def convert_price(price, levels):
    return convert_value('price', price, levels)


# def convert_nearby(nearby, levels):
#     return convert_value('nearby', nearby, levels)


def convert_value(prefix, value, levels):
    for lv in levels:
        prev = 0
        if levels.index(lv) > 0:
            prev = levels[levels.index(lv) - 1]
        if value > prev and value <= lv:
            return prefix + ' >' + str(prev) + ' and <=' + str(lv)

    return prefix + ' >=' + str(levels[len(levels) - 1])

header = f_build[0]
schema = header.split('\t')
idx = 0
for col in schema:
    print idx,':',col
    idx += 1

# 將讀入的資料作整理，變成後續 build tree 需要的資料集的格式
from operator import itemgetter

origin_datas = []
filter_datas = []
traindata = []

for row in f_build[1:]:
    cols = row.split('\t')
    origin_datas.append(cols)
    if cols[4] == '住' and cols[3] != '0' and cols[1] != '土地' \
            and (('層' in cols[9] and '夾層' not in cols[9]) or '全部' in cols[9]) \
            and cols[14] != '':
        orec = []
        orec.append(cols[0])
        orec.append(float(cols[3]))
        orec.append(int(get_layer(cols[9])))
        orec.append(int(cols[10]))
        orec.append(float(get_years(cols[14])))
        orec.append(get_have_flag(cols[20]))
        orec.append(get_have_flag(cols[21]))
        orec.append(get_park('1', cols[24]))
        orec.append(get_have_flag(count_nearby(cols[2])))
        orec.append(int(cols[22]))
        filter_datas.append(orec)

# 依資料分佈來區分間
square_levels = cal_levels(filter_datas, 1, 5)
floor_levels = cal_levels(filter_datas, 2, 5)
total_floor_levels = cal_levels(filter_datas, 3, 5)
year_levels = cal_levels(filter_datas, 4, 5)
nearby_levels = cal_levels(filter_datas, 8, 5)
# price_levels = cal_levels(filter_datas,8,10)

for cols in filter_datas:
    rec = []
    rec.append(get_address('1', cols[0]))  # 鄉鎮市區
    rec.append(convert_square(cols[1], square_levels))  # 租賃總面績 (轉區間代號)
    rec.append(convert_floor(False, cols[2], floor_levels))  # 租賃層次 (轉代號)
    rec.append(convert_floor(True, int(cols[3]), total_floor_levels))  # 總樓層
    rec.append(convert_house_age(float(cols[4]), year_levels))  # 建物完成日 (屋齡)
    rec.append(cols[5])  # 管理員
    rec.append(cols[6])  # 家具
    rec.append(cols[7])                                             #車位類別
    rec.append(cols[8])              # 附近交通
    rec.append(int(cols[9]))
    #     rec.append(convert_price(int(cols[8]),price_levels))            #租金總額元
    traindata.append(rec)
    #         rec.append(cols[]) #
    #         rec.append(cols[]) #
    #         rec.append(cols[]) #

print square_levels
print floor_levels
print total_floor_levels
print year_levels
print nearby_levels
# print price_levels

class decisionnode:
    def __init__(self,col=-1,value=None,results=None,tb=None,fb=None):
        self.col=col
        self.value=value
        self.results=results
        self.tb=tb
        self.fb=fb

# Divides a set on a specific column. Can handle numeric
# or nominal values
def divideset(rows,column,value):
    # Make a function that tells us if a row is in
    # the first group (true) or the second group (false)
    split_function=None
    if isinstance(value,int) or isinstance(value,float):
        split_function=lambda row:row[column]>=value
    else:
        split_function=lambda row:row[column]==value

    # Divide the rows into two sets and return them
    set1=[row for row in rows if split_function(row)]
    set2=[row for row in rows if not split_function(row)]
    return (set1,set2)

# Probability that a randomly placed item will
# be in the wrong category
def giniimpurity(rows):
    total=len(rows)
    counts=uniquecounts(rows)
    imp=0
    for k1 in counts:
        p1=float(counts[k1])/total
        for k2 in counts:
            if k1==k2: continue
            p2=float(counts[k2])/total
            imp+=p1*p2
    return imp

# Entropy is the sum of p(x)log(p(x)) across all
# the different possible results
def entropy(rows):
    from math import log
    log2=lambda x:log(x)/log(2)
    results=uniquecounts(rows)
    # Now calculate the entropy
    ent=0.0
    for r in results.keys():
        p=float(results[r])/len(rows)
        ent=ent-p*log2(p)
    return ent

def variance(rows):
    if len(rows)==0: return 0
    data=[float(row[len(row)-1]) for row in rows]
    mean=sum(data)/len(data)
    variance=sum([(d-mean)**2 for d in data])/len(data)
    return variance


# Create counts of possible results (the last column of
# each row is the result)
def uniquecounts(rows):
    results={}
    for row in rows:
        # The result is the last column
        r=row[len(row)-1]
        if r not in results: results[r]=0
        results[r]+=1
    return results

def buildtree(rows,scoref=entropy):
    if len(rows)==0: return decisionnode()
    current_score=scoref(rows)

    # Set up some variables to track the best criteria
    best_gain=0.0
    best_criteria=None
    best_sets=None

    column_count=len(rows[0])-1

    for col in range(0,column_count):
        # Generate the list of different values in
        # this column
        column_values={}
        for row in rows:
            column_values[row[col]]=1
        # Now try dividing the rows up for each value
        # in this column
        for value in column_values.keys():
            (set1,set2)=divideset(rows,col,value)
            # Information gain
            p=float(len(set1))/len(rows)
            gain=current_score-p*scoref(set1)-(1-p)*scoref(set2)
            if gain>best_gain and len(set1)>0 and len(set2)>0:
                best_gain=gain
                best_criteria=(col,value)
                best_sets=(set1,set2)
    # Create the sub branches
    if best_gain>0:
        trueBranch=buildtree(best_sets[0])
        falseBranch=buildtree(best_sets[1])
        return decisionnode(col=best_criteria[0],value=best_criteria[1],
                        tb=trueBranch,fb=falseBranch)
    else:
        return decisionnode(results=uniquecounts(rows))

dtree = buildtree(traindata,scoref=variance)

def printtree(tree,indent=''):
    # Is this a leaf node?
    if tree.results!=None:
        print str(tree.results)
    else:
        # Print the criteria
        print str(tree.col)+':'+str(tree.value)+'? '

        # Print the branches
        print indent+'T->',
        printtree(tree.tb,indent+'  ')
        print indent+'F->',
        printtree(tree.fb,indent+'  ')


def getwidth(tree):
    if tree.tb==None and tree.fb==None: return 1
    return getwidth(tree.tb)+getwidth(tree.fb)

def getdepth(tree):
    if tree.tb==None and tree.fb==None: return 0
    return max(getdepth(tree.tb),getdepth(tree.fb))+1

printtree(dtree)

from PIL import Image,ImageDraw

def drawtree(tree,jpeg='tree.jpg',ext='JPEG'):
    w=getwidth(tree)*100
    h=getdepth(tree)*100+120

    img=Image.new('RGB',(w,h),(255,255,255))
    draw=ImageDraw.Draw(img)

    drawnode(draw,tree,w/2,20)
    img.save(jpeg,ext)

def drawnode(draw,tree,x,y):
    if tree.results==None:
        # Get the width of each branch
        w1=getwidth(tree.fb)*100
        w2=getwidth(tree.tb)*100

        # Determine the total space required by this node
        left=x-(w1+w2)/2
        right=x+(w1+w2)/2

        # Draw the condition string
        draw.text((x-20,y-10),str(tree.col)+':'+str(tree.value),(0,0,0))

        # Draw links to the branches
        draw.line((x,y,left+w1/2,y+100),fill=(255,0,0))
        draw.line((x,y,right-w2/2,y+100),fill=(255,0,0))

        # Draw the branch nodes
        drawnode(draw,tree.fb,left+w1/2,y+100)
        drawnode(draw,tree.tb,right-w2/2,y+100)
    else:
        txt=' \n'.join(['%s:%d'%v for v in tree.results.items()])
        draw.text((x-20,y),txt,(0,0,0))

drawtree(dtree,jpeg='treeview_conti.png',ext='PNG')