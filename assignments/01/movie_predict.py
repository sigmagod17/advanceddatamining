import os
import collections
from math import sqrt

# Read file
with open(os.getcwd() + '/../ml-1m/movies.dat', 'r' , encoding='ISO-8859-1') as f:
    f_movies = f.readlines()

with open(os.getcwd() + '/../ml-1m/ratings.dat', 'r') as f:
    f_ratings = f.readlines()

movies = {}
for line in f_movies:
    (mid, title, genres) = line.split('::')
    movies[mid] = title
    # Pressent Data
    # 1 Toy Story (1995) Animation|Children's|Comedy
    # 2 Jumanji (1995) Adventure|Children's|Fantasy

# Combine file to generate like Key,Value Format
# define a rating dictionary
critics = {}
for line in f_ratings:
    (uid, mid, rating, timestamp) = line.split('::')
    if uid not in critics:
        critics[uid] = {}
    critics[uid][movies[mid]] = float(rating)


# print(critics['5988'])

# Data Transpose
def transformPrefs(prefs):
    '''
    Transform the recommendations into a mapping where persons are described
    with interest scores for a given title e.g. {title: person} instead of
    {person: title}.
    '''
    result = {}
    for movies in prefs:
        for item in prefs[movies]:
            result.setdefault(item, {})
            # Flip item and person
            result[item][movies] = prefs[movies][item]
    return result


# similarity(pearson)
def sim_pearson(prefs, p1, p2):
    # Get the list of mutually rated items
    si = {}
    for item in prefs[p1]:
        if item in prefs[p2]:
            si[item] = 1
    # If they are no ratings in common, return 0
    if len(si) == 0:
        return 0
    # Sum calculations
    n = len(si)
    # Sums of all the preferences
    sum1 = sum([prefs[p1][it] for it in si])
    sum2 = sum([prefs[p2][it] for it in si])
    # Sums of the squares
    sum1Sq = sum([pow(prefs[p1][it], 2) for it in si])
    sum2Sq = sum([pow(prefs[p2][it], 2) for it in si])
    # Sum of the products
    pSum = sum([prefs[p1][it] * prefs[p2][it] for it in si])
    # Calculate r (Pearson score)
    num = pSum - sum1 * sum2 / n
    den = sqrt((sum1Sq - pow(sum1, 2) / n) * (sum2Sq - pow(sum2, 2) / n))
    if den == 0:
        return 0
    r = num / den
    return r


# similarity(distance)
def sim_distance(prefs, p1, p2):
    '''
    Returns a distance-based similarity score for person1 and person2.
    '''

    # Get the list of shared_items
    si = {}
    for item in prefs[p1]:
        if item in prefs[p2]:
            si[item] = 1
    # If they have no ratings in common, return 0
    if len(si) == 0:
        return 0
    # Add up the squares of all the differences
    sum_of_squares = sum([pow(prefs[p1][item] - prefs[p2][item], 2) for item in prefs[p1] if item in prefs[p2]])
    return 1 / (1 + sqrt(sum_of_squares))


# Top10 Pearson Similarity
def topMatchespearson(prefs, movies, n=10, similarity=sim_pearson):
    '''
    Returns the best matches for person from the prefs dictionary.
    Number of results and similarity function are optional params.
    '''
    scores = [(similarity(prefs, movies, other), other) for other in prefs
              if other != movies]
    scores.sort()
    scores.reverse()

    return scores[0:n]


# Top10 Distance Similarity
def topMatchesdistance(prefs, movies, n=10, similarity=sim_distance):
    '''
    Returns the best matches for person from the prefs dictionary.
    Number of results and similarity function are optional params.
    '''
    scores = [(similarity(prefs, movies, other), other) for other in prefs
              if other != movies]
    scores.sort()
    scores.reverse()

    return scores[0:n]


def calculateSimilarItemspearson(prefs, item_search, n=9999, similarity=sim_pearson):
    result = {}
    # Invert the preference matrix to be item-centric
    itemPrefs = transformPrefs(prefs)
    for item in itemPrefs:
        # Find the most similar items to this one
        if item in item_search:
            similar_item = topMatchespearson(itemPrefs, item, n=n, similarity=sim_pearson)
            result[item] = similar_item
    return result


def calculateSimilarItemsdistance(prefs, item_search, n=9999, similarity=sim_distance):
    result = {}
    # Invert the preference matrix to be item-centric
    itemPrefs = transformPrefs(prefs)
    for item in itemPrefs:
        # Find the most similar items to this one
        if item in item_search:
            similar_item = topMatchesdistance(itemPrefs, item, n=n, similarity=sim_distance)
            result[item] = similar_item
    return result


sim_list_pearson = calculateSimilarItemspearson(critics, {'Toy Story (1995)': ''})
sim_list_distance = calculateSimilarItemsdistance(critics, {'Toy Story (1995)': ''})

print("Pearson Similarity")
for list_print_pearson in sim_list_pearson['Toy Story (1995)'][0:30]:
    print(list_print_pearson)
print("\n")
print("Distance Similarity")
for list_print_distance in sim_list_distance['Toy Story (1995)'][0:30]:
    print(list_print_distance)

# od = collections.OrderedDict(sorted(movies.items()))
# for movies_key, movies_value in od.items():
#     with open("Output.txt", "a") as text_file:
#         text_file.write(movies_key + ' ' + movies_value +'\n')
