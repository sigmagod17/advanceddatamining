
# coding: utf-8

# In[1]:


import os
import jieba
import ijson
import sys
import collections
from string import whitespace
import json
import jieba.posseg as pseg


# In[2]:

reload(sys)
sys.setdefaultencoding('utf-8')


# In[6]:
jieba.enable_parallel(4)
jieba.load_userdict(os.getcwd() + '/dict/dict_wiki.txt.big')
stopwordset = set()
with open(os.getcwd()+'/dict/stopwords.txt','rb') as sw:
    for line in sw:
        stopwordset.add(line.strip('\n'))


# In[ ]:

dict = {}
for prefix, types, value in ijson.parse(open('./wiki_data.json')):
    words = pseg.cut(str(value))
    # words = jieba.cut(str(value))
    count = 0
    for word in words:
        # if word.flag == 'n' or word.flag == 'nr' or word.flag == 'nrfg' or word.flag == 'nrt' or word.flag == 'nt' or word.flag == 'nz':
        if word.flag == 'n' or word.flag == 'nrfg' or word.flag == 'nrt' or word.flag == 'nt' or word.flag == 'nz':
            word = word.word.encode('utf-8')  # 要加這行，判斷停用詞才會準

            if word not in stopwordset:
                if word != '\n' :
                # print word.decode('utf-8')
                # dict[str(prefix)] = word.decode('utf-8')
                    if dict.has_key(word.decode('utf-8')):
                        dict[word.decode('utf-8')].append(str(prefix))
                    else:
                        dict.update({word.decode('utf-8'):[str(prefix)]})


# In[ ]:

sorted(dict.keys())

json_result = {}
for key,value in dict.items():
    value = list(set(value))
    value.sort()
    json_result.update({key:value})

jsonString = json.dumps(json_result,ensure_ascii=False,indent=4)
fname = "wiki_data_keyvalue.json"
fout = open(fname,'w')
fout.write(jsonString)
fout.close()


# In[ ]:



