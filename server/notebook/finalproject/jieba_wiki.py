#encoding=utf-8
# -*- coding: utf-8 -*-

import os

# import ijson
import jieba
import json
import codecs


#import jieba.posseg as pseg
#from jieba.posseg import cut



def main():
    jieba.set_dictionary('dict/NameDict_Ch_v2.txt')
    #jieba.load_userdict('dict/dict_wiki.txt.big')
    stopwordset = set()
    with open(os.getcwd()+'/dict/stopwords.txt','rb') as sw:
        for line in sw:
            stopwordset.add(line.strip('\n'))

    dict = {}
    content = open('wiki_data.json','rb').read()
    articles = json.loads(content)
    for art_id in articles:
        words = jieba.cut(articles[art_id])
        for word in words:
            if word not in stopwordset:
                if word != '\n':
                    print word.encode('utf-8')
                    if dict.has_key(word):
                        dict[word].append(art_id)
                    else:
                        dict.update({word: [art_id]})
    json_result = {}
    for key,value in dict.items():
        value = list(set(value))
        value.sort()
        json_result.update({key:value})


    outfile = codecs.open("wiki_data_keyvalue_nopseg_1.json", "w", "utf-8")
    jsonString = json.dumps(json_result, ensure_ascii=False, indent=4)
    outfile.write(jsonString)
    outfile.close()


if __name__ == '__main__':
    main()
