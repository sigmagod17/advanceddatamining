
# coding: utf-8

# In[10]:


import os
import jieba
import ijson
import sys
import collections
from string import whitespace
import json


# In[11]:

reload(sys)
sys.setdefaultencoding('utf-8')


# In[13]:

#jieba.set_dictionary(os.getcwd() + '/dict/mark_replaced.txt')
jieba.set_dictionary(os.getcwd() + '/dict/dict_all.txt')
#jieba.set_dictionary(os.getcwd() + '/dict/dict.txt.big')
jieba.load_userdict(os.getcwd() + '/dict/dict_custom.txt')

stopwordset = set()
with open(os.getcwd()+'/dict/stopwords.txt','rb') as sw:
    for line in sw:
        stopwordset.add(line.strip('\n'))


jieba.enable_parallel(4)

dict = {}
for prefix, types, value in ijson.parse(open('./wiki_data.json')):
#for prefix, types, value in ijson.parse(open('./jotest.json')):
    words = jieba.cut(str(value))
    count = 0
    for word in words:
        word = word.encode('utf-8')  # 要加這行，判斷停用詞才會準
        if word not in stopwordset:
            if word != '\n' :
            # print word.decode('utf-8')
            # dict[str(prefix)] = word.decode('utf-8')
                if dict.has_key(word.decode('utf-8')):
                    dict[word.decode('utf-8')].append(str(prefix))
                else:
                    dict.update({word.decode('utf-8'):[str(prefix)]})


# In[ ]:

sorted(dict.keys())

json_result = {}
json_idx = {}
for key,value in dict.items():
    value = list(set(value))
    value.sort()
    #json_result.update({key:value})
    if json_idx.has_key(len(key)):
	json_idx[len(key)].update({key:value})
    else:
	json_idx.update({len(key):{key:value}})

# In[ ]:

jsonString = json.dumps(json_idx,ensure_ascii=False,indent=4)
fname = "wiki_data_keypair_idx.json"
fout = open(fname,'w')
fout.write(jsonString)
fout.close()


# In[ ]:



