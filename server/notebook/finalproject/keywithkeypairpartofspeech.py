# -*- coding: utf-8 -*-
import os
import json
import sys

reload(sys)

sys.setdefaultencoding('utf-8')

with open('./wiki_data_keypair_jo.json', 'r') as f:
    data = json.load(f)

def get_count(data):
    dict = {}
    for index, (key, value) in enumerate(data.iteritems()):
        dict_sub = {}
        for indexs, (keys, values) in enumerate(data.iteritems()):
            count = 0
            if indexs != index:
                for value_key in value:
                    for values_key in values:
                        if value_key == values_key:
                            count = count + 1
            if count > 0:
                # if dict.has_key(key):
                dict_sub.update({keys: str(count)})
                dict.update({key: dict_sub})

    return dict

jsonString = json.dumps(get_count(data), ensure_ascii=False, indent=4)
fname = "wiki_data_keywithkeypartofspeech.json"
fout = open(fname, 'w')
fout.write(jsonString)
fout.close()
